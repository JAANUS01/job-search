// const numbers = [1, 3, 5, 7, 2, 9, 11, 6];

// console.log(numbers.filter((number) => number > 6));

const jobs = [
  { title: "developer", organization: "microsoft" },
  { title: "programmer", organization: "google" },
  { title: "angular developer", organization: "microsoft" },
];

console.log(jobs.filter((job) => job.organization === "microsoft"));
